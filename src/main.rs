use rand::Rng;
use std::cmp::Ordering;
use std::io;
fn start_guess_game(chances: u8) {
    if chances == 0 {
        return;
    }
    let mut user_input = String::new();

    println!("My friend enter your answer");
    io::stdin()
        .read_line(&mut user_input)
        .expect("hoho something is wrong");
    println!("Indeed you wrote {user_input}");
    let user_input: u32 = match user_input.trim().parse() {
        Ok(num) => num,
        Err(_) => {
            println!("type a number my boy");
            start_guess_game(chances);
            0
        }
    };
    let secret_num = rand::thread_rng().gen_range(0..=100);
    println!("secret number is {secret_num}");
    match user_input.cmp(&secret_num) {
        Ordering::Less => {
            println!("Too low");
            start_guess_game(chances - 1)
        }
        Ordering::Equal => println!("Good job boy"),
        Ordering::Greater => {
            println!("Too high");
            start_guess_game(chances - 1)
        }
    }
}
fn main() {
    start_guess_game(3)
}
